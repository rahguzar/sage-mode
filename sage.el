;;; sage.el --- A front-end for Sage Math -*- lexical-binding: t -*-

;; URL: https://codeberg.org/rahguzar/sage-mode
;; Package-Requires: ((emacs "25.1") (compat "28.1"))
;; Keywords: languages, processes, tools
;; Version: 0.1

;;; License
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This package provides a front end for Sage (http://www.sagemath.org/)
;; and a major mode derived from python-mode (sage-mode).

;; To use this package, check the return value of (executable-find "sage").
;; If (executable-find "sage") is a string, you are ready to use this package.

;; Then you can run Sage process in Emacs by M-x sage-run.

;; This is a work in progress. There goal here to replace `sage-shell'
;; with something that is much smaller, as close as possible to vanilla
;; `comint-mode' and `inferior-python-mode' for the repl and directly use
;; the facilities provided by IPython bundled by sage for most of the tasks.

;;; Code:

(require 'cl-lib)
(require 'comint)
(require 'python)
(require 'compile)
(require 'outline)
(require 'compat)
(require 'info)

;;; Global variables for users
(defgroup sage
  nil "Emacs interface to sagemath."
  :group 'languages)

(defcustom sage-executable nil
  "Name of the Sage executable.
If the Sage executable in your PATH i.e. (exeutable-find \"sage\") is non-nil,
then you do not have to set this variable."
  :type '(choice (string :tag "Executable for sage")
                 (const :tag "Not specified" nil)))

(defcustom sage-history-file nil
  "If non nil, then `comint-input-ring' is saved to this file.
This happens when the Sage process exits."
  :type '(choice (file :tag "file")
                 (const :tag "Off" nil)))

(defcustom sage-info-manual "sage"
  "The name of the sage info manual."
  :type 'string)

(defcustom sage-info-prettify-symbols
  '(("\\ZZ" . ?ℤ)
    ("\\QQ" . ?ℚ)
    ("\\RR" . ?ℝ)
    ("\\CC" . ?ℂ)
    ("\\infty" . ?∞))
  "Additional elements of `prettify-symbols-alist' for sage info manual."
  :type '(repeat (cons string character)))

;;; sage-shell
(defun sage-executable ()
  "Find the sage executable and set the variable `sage-executable' if it is unset."
  (or sage-executable
      (when-let ((exe (executable-find "sage")))
        (file-truename exe))))

;;;; Python code for completion, eldoc and sending code
(defvar sage-setup-code
  "\
get_ipython().history_manager.enabled = False
In.clear()
get_ipython().history_manager.reset()

import warnings

def __PYTHON_EL_eval(source, filename):
    import ast, sys
    from sage.repl.preparse import preparse_file
    if isinstance(source, str):
        source = preparse_file(source)
    else:
        source = preparse_file(source.decode())
    if sys.version_info[0] == 2:
        from __builtin__ import compile, eval, globals
    else:
        from builtins import compile, eval, globals
    try:
        p, e = ast.parse(source, filename), None
    except SyntaxError:
        t, v, tb = sys.exc_info()
        sys.excepthook(t, v, tb.tb_next)
        return
    if p.body and isinstance(p.body[-1], ast.Expr):
        e = p.body.pop()
    try:
        g = globals()
        exec(compile(p, filename, 'exec'), g, g)
        if e:
            return eval(compile(ast.Expression(e.value), filename, 'eval'), g, g)
    except Exception:
        t, v, tb = sys.exc_info()
        sys.excepthook(t, v, tb.tb_next)

from IPython.terminal.prompts import Prompts, Token
class __SAGEPrompt(type(get_ipython().prompts)):
     redirect_happening = False
     initial_prompts = get_ipython().prompts
     def in_prompt_tokens(self, cli=None):
         if self.redirect_happening:
             self.redirect_happening = False
             return [(Token.Prompt,'***** COMINT REDIRECT ENDED *****')]
         else:
             return self.initial_prompts.in_prompt_tokens()
get_ipython().prompts = __SAGEPrompt(get_ipython())

from IPython.core.completer import IPCompleter
__IP_completer = IPCompleter(shell=get_ipython() , namespace=locals())
__IP_completer.jedi_compute_type_timeout = int(0)

def __SAGE_annotate(obj):
    import inspect
    doc = None
    ann = ''
    try:
        if isinstance(obj, str):
            obj = eval(obj, globals())
            doc = inspect.getdoc(obj)
        if doc:
            ann = doc.splitlines()[0]
        if callable(obj):
            target = None
            if inspect.isclass(obj) and hasattr(obj, '__init__'):
                target = obj.__init__
                objtype = 'class'
            else:
                target = obj
                objtype = 'def'
            if target:
                args = str(inspect.signature(obj))
                name = obj.__name__
                ann = f'{objtype} {name}{args}' + ': ' + ann
    except:
        ann = ' '
    return ann

def __SAGE_complete(str):
    In.pop()
    with warnings.catch_warnings():
        warnings.simplefilter('ignore')
        get_ipython().prompts.redirect_happening = True
        print(*[x + '        ' + __SAGE_annotate(x) for x in __IP_completer.all_completions(str)], sep='\\n')

def __SAGE_complete_import(str,offset):
    In.pop()
    res = get_ipython().complete('', str, offset)
    get_ipython().prompts.redirect_happening = True
    print(res[0]+'        ')
    print(*[r + '        ' for r in res[1]], sep='\\n')

def  __PYDOC_get_help(obj):
    In.pop()
    return __SAGE_annotate(obj)

def __SAGE_get_doc(str):
    In.pop()
    get_ipython().prompts.redirect_happening = True
    get_ipython().run_line_magic('pinfo', str)

def __SAGE_get_full_name(obj):
    In.pop()
    get_ipython().prompts.redirect_happening = True
    print(obj.__module__ + '.' + obj.__qualname__ + '\\n')
"
  "Code used to evaluate statements in sage repl.
Default value is adapted from `python-shell-eval-setup-code' with the
difference that this in addition preparses the sage input.")

;;;; Comint filters
(defvar-local sage--process-busy-p nil)

(defvar-local sage-next-redirect-command nil
  "A cons (FUN . ARGS). FUN will be called with ARGS when redirect finishes.")

(defvar-local sage--saved-input nil
  "Saved input to restore after the process.")

(defun sage--input-filter (_)
  "Hook added to `comint-input-filter-functions'."
  (setq sage--process-busy-p t))

(defun sage--output-filter (output)
  "Restore the input that was previously saved when OUTPUT has prompt."
  (when (string-match (rx (regexp comint-prompt-regexp) eos) output)
    (setq sage--process-busy-p nil)
    (when sage--saved-input
      (comint-goto-process-mark)
      (insert sage--saved-input)
      (setq sage--saved-input nil))))

(defun sage--redirect-done ()
  "Addition to `comint-redirect-hook'."
  (with-current-buffer comint-redirect-output-buffer
    (goto-char (point-min)))
  (when sage-next-redirect-command
    (let ((com sage-next-redirect-command))
      (setq sage-next-redirect-command nil)
      (run-with-idle-timer 0.01 nil (lambda () (apply (car com) (cdr com)))))))

(defun sage--save-input ()
  "Save the current input."
  (setq sage--saved-input (progn (comint-goto-process-mark)
                                 (delete-and-extract-region (point) (point-max)))))

(defvar sage-rich-output-mode nil)
(declare-function sage-rich-output-mode "sage-rich")

(defun sage-first-prompt-setup ()
  "Setup the interactive session using `python-shell-first-prompt-hook'."
  (python-shell-send-string-no-output sage-setup-code)
  (when (default-value sage-rich-output-mode)
    (sage-rich-output-mode))
  (add-text-properties (line-beginning-position) (point)
                       `(rear-nonsticky
                         ,comint--prompt-rear-nonsticky
                         field boundary
                         inhibit-line-move-field-capture t))
  (eldoc-mode))

;;;; Sage shell mode
(define-derived-mode sage-shell-mode inferior-python-mode
  "Sage Repl" "Execute Sage commands interactively."
  :group 'sage

  (setq-local comint-prompt-regexp (rx (regexp comint-prompt-regexp) eos)
              comint-redirect-perform-sanity-check nil
              comint-redirect-completed t
              comint-input-ring-file-name sage-history-file
              comint-input-ignoredups t)

  (setq-local python-shell-completion-native-enable nil)

  (comint-read-input-ring t)

  (setq-local completion-at-point-functions '(sage-completions-at-point t))

  (add-hook 'kill-buffer-hook #'comint-write-input-ring)

  (add-hook 'comint-input-filter-functions #'sage--input-filter nil t)
  (add-hook 'comint-output-filter-functions #'sage--output-filter t t)
  (add-hook 'comint-redirect-filter-functions #'ansi-color-apply nil t)
  (add-hook 'comint-redirect-hook #'sage--redirect-done nil t)
  (add-hook 'python-shell-first-prompt-hook #'sage-first-prompt-setup nil t)
  (add-hook 'eldoc-documentation-functions #'sage-eldoc-function nil t))

;;;; Running sage processes
(defvar python-shell--interpreter)
(defvar python-shell--interpreter-args)

(defun sage--run (buffer-name display &rest switches)
  "Run Sage.
If BUFFER-NAME is a string, it will be the name of the process buffer. If it
is any other non-nil value a new buffer based on `*Sage*' is used. If it is
nil the buffer `*Sage*' is used and resued if it already present.

If buffer thus found has no associated process a sage process is satrted.
SWITCHES are the arguments passed to it as in `make-comint-in-buffer'.
If DISPLAY is non-nil the buffer is displayed."
  (let* ((proc (python-shell-get-process-name buffer-name))
         (buf (get-buffer-create (if (stringp buffer-name)
                                     buffer-name
                                   (format "*%s*" proc)))))
    (if display (display-buffer buf))
    (unless (get-buffer-process buf)
      (apply #'make-comint-in-buffer proc buf (sage-executable) nil switches)
      (with-current-buffer buf
        (let ((inhibit-read-only t)) (erase-buffer))
        (let ((python-shell--interpreter "sage")
              (python-shell--interpreter-args "--simple-prompt")
              (python-shell-prompt-detect-enabled nil)
              (python-shell-interpreter-interactive-arg "")
              (python-shell-prompt-regexp "sage: "))
          (sage-shell-mode))))
    buf))

;;;###autoload
(defun sage-run (arg &optional display)
  "Run sage. If ARG is non-nil start a dedicated sage process.
If DISPLAY is non-nil display the resulting buffer."
  (interactive (list (or current-prefix-arg python-shell-dedicated) t))
  (sage--run arg display "--simple-prompt"))

(defun sage-restart ()
  "Restart the Sage process for current buffer."
  (interactive)
  (when-let ((buf (python-shell-get-buffer))
             (proc (get-buffer-process buf)))
    (with-current-buffer buf
      (comint-send-eof) (set-process-buffer proc nil) (kill-process proc)))
  (sage-run (get-buffer (format "*%s*" (python-shell-get-process-name t)))))

(defvar-local sage--completion-table nil)

(defun sage-busy-p (buf)
  "Return non-nil if sage process in BUF is busy."
  (or (not (buffer-local-value 'comint-redirect-completed buf))
      (buffer-local-value 'sage--process-busy-p buf)))

;;; Completion
(defvar sage--current
  "Internal variable for redirection machinery.")

(defun sage--redirect-send (command output-buffer process echo no-display)
  "Version of `comint-redirect-send-command-to-process'.
Which see for COMMAND, OUTPUT-BUFFER, PROCESS, ECHO and NO-DISPLAY.
The COMMAND must output ***** COMINT REDIRECT ENDED ***** as the last line."
  (with-current-buffer (process-buffer process)
    (if-let (((not (sage-busy-p (current-buffer))))
             (comint-prompt-regexp (rx bol "***** COMINT REDIRECT ENDED *****")))
        (comint-redirect-send-command-to-process command output-buffer process echo no-display)
      (setq sage-next-redirect-command
            `(sage--redirect-send ,command ,output-buffer ,process ,echo ,no-display)))))

(defun sage--completions-setup (command term proc)
  "Set up the buffer for completions for TERM using COMMAND from PROC."
  (let ((buf (get-buffer-create (concat " *sage_completions* " (process-name proc)) t)))
    (with-current-buffer buf
      (erase-buffer)
      (setq-local sage--current term))
    (sage--redirect-send command buf proc nil t)
    buf))

(defun sage--try-collect-completions (buf term proc)
  "Collect completions for TERM from BUF generated by PROC."
  (let* ((result)
         (proc-buf (process-buffer proc))
         (continue t))
    (while-no-input
      (while continue
        (with-current-buffer buf
          (goto-char 1)
          (while (and (> (buffer-size) 1) (equal term sage--current))
            (if-let ((match (search-forward "        " (line-end-position) t)))
                (push (propertize (string-trim-right (delete-and-extract-region
                                                      (pos-bol) match))
                                  :annotation (string-trim-right (delete-and-extract-region
                                                                  (point) (1+ (pos-eol)))))
                      result)
              (delete-region (pos-bol) (min (point-max) (1+ (pos-eol))))))
          (if (buffer-local-value 'comint-redirect-completed proc-buf)
              (setq continue nil)
            (accept-process-output proc)))))
    (nreverse result)))

(defun sage-completion-table (&optional proc)
  "Return a completion table for completions from sage process PROC.
The return value can be passed as `COLLECTION' argument of standard completion
functions like `completing-read'.

The design is taken from `completion-table-with-cache' but accounts
for the fact that we get new completions after encountering a new dot."
  (let* (last-arg
         result
         (proc (or proc (python-shell-get-process)))
         buf
         (new-fun
          (lambda (arg)
            (setq arg (string-trim-right arg (rx (* (not ?.)))))
            (unless (equal arg last-arg)
              (setq result nil)
              (setq buf (sage--completions-setup (concat "__SAGE_complete('" arg "')") arg  proc))
              (setq last-arg arg))
            (and buf (process-live-p proc)
                 (cl-callf nconc result (sage--try-collect-completions buf arg proc)))
            result)))
    (completion-table-dynamic new-fun)))

(defun sage-completions-at-point ()
  "Completions at point from the sage process."
  (with-syntax-table python-dotty-syntax-table
    (if-let ((proc (python-shell-get-process))
             (proc-buf (process-buffer proc)))
        (if (not (save-excursion (goto-char (line-beginning-position))
                                 (looking-at (rx (or "from " "import ")))))
            (when-let ((symbol (thing-at-point 'symbol))
                       ((not (string-match (rx bos ?.) symbol)))
                       (bounds (bounds-of-thing-at-point 'symbol)))
              (list (car bounds) (cdr bounds)
                    (or (buffer-local-value 'sage--completion-table proc-buf)
                        (with-current-buffer proc-buf
                          (setq sage--completion-table (sage-completion-table))))
                    :annotation-function
                    (lambda (cand) (propertize (get-text-property 0 :annotation cand)
                                          'face font-lock-comment-face))))
          (when-let ((arg (buffer-substring (line-beginning-position)
                                            (line-end-position)))
                     (completions (sage--try-collect-completions
                                   (sage--completions-setup
                                    (format "__SAGE_complete_import('%s' , %s)"
                                            arg
                                            (- (point) (line-beginning-position)))
                                    arg proc)
                                   arg proc)))
            (list (- (point) (length (car completions))) (point) (cdr completions))))
      (message "Current buffer is not associated to any sage process. Completions are not available."))))

;;; Sage Mode
(defun sage-outline-level ()
  "Value of variable `outline-level' for `sage-mode'."
  (- (match-end 0) (match-beginning 0) 4))

(defvar sage-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map [remap run-python] #'sage-run)
    (define-key map [remap python-eldoc-at-point] #'sage-lookup-doc)
    (define-key map (kbd "C-c C-b") #'sage-send-heading-body)
    map))

;;;###autoload
(define-derived-mode sage-mode python-mode "Sage"
  :group 'sage
  (add-hook 'completion-at-point-functions #'sage-completions-at-point nil t)
  (add-hook 'eldoc-documentation-functions #'sage-eldoc-function nil t)
  (remove-hook 'eldoc-documentation-functions #'python-eldoc-function t)
  (setq-local python-shell-buffer-name "Sage"
              python-shell-interpreter (sage-executable)
              python-eldoc-get-doc t
              python-shell-interpreter-args "--simple-prompt"
              python-eldoc-setup-code ""
              outline-regexp "[ 	]*####* [^ 	\n]"
              outline-level 'sage-outline-level))

;;;###autoload
(cl-pushnew `(,(rx ".sage" eos) . sage-mode) auto-mode-alist :test #'equal)

;;; Look up documentation
(defun sage-lookup-doc (symbol &optional proc display)
  "Look up documentation for the SYMBOL.
It is the symbol at point when called interactively. PROC is the sage process.
If DISPLAY is non-nil, buffer is displayed."
  (interactive (list (python-info-current-symbol) (python-shell-get-process) t))
  (let ((buf (get-buffer-create "*Sage Documentation*"))
        (proc (or proc (python-shell-get-process))))
    (prog1 buf
      (with-current-buffer buf
        (erase-buffer)
        (setq mode-line-format nil)
        (visual-line-mode)
        (font-lock-mode))
      (sage--redirect-send (format "__SAGE_get_doc('%s')" symbol)
                           buf proc nil t)
      (when display (pop-to-buffer buf)))))

(defun sage-eldoc-function (&rest _)
  "Eldoc function for sage."
  (unless (sage-busy-p (process-buffer (python-shell-get-process)))
    (with-timeout (python-eldoc-function-timeout) (python-eldoc-function))))

;;; Send code to sage process
(defun sage-send-line (proc)
  "Send current line to the sage process PROC."
  (interactive (list (python-shell-get-process)))
  (let ((line (buffer-substring (line-beginning-position) (line-end-position))))
    (with-current-buffer (process-buffer proc)
      (sage--save-input)
      (insert line)
      (comint-send-input))))

(defun sage-send-heading-body ()
  "Send the body of current heading to the sage process.
Precisely, if the point is on a heading, send the part of the buffer from the
start of the heading to just before the beginning of next the heading at the
same or greater level. If the point is not on a heading use the previous
visible heading."
  (interactive)
  (save-excursion
    (let ((beg (if (outline-on-heading-p)
                   (line-beginning-position)
                 (outline-previous-heading) (point)))
          (end (progn (outline-get-next-sibling)
                      (line-end-position 0))))
      (python-shell-send-region beg end))))

(defun sage--send-region (start end &optional send-main msg
                                no-cookie)
  "Send the region delimited by START and END to inferior Python process.
SEND-MAIN, MSG and NO-COOKIE are as in `python-shell-send-region'."
  (when (eq major-mode #'sage-mode)
    (let* ((string (python-shell-buffer-substring start end (not send-main)
                                                  no-cookie))
           (process (python-shell-get-process-or-error msg))
           (original-string (buffer-substring-no-properties start end))
           (_ (string-match "\\`\n*\\(.*\\)" original-string)))
      ;; Recalculate positions to avoid landing on the wrong line if
      ;; lines have been removed/added.
      (with-current-buffer (process-buffer process)
        (sage--save-input)
        (insert (truncate-string-to-width
                 (format "# %s: %s..."
                         this-command
                         (match-string 1 original-string))
                 (- (window-width (get-buffer-window)) 15)
                 nil nil "..."))
        (let ((comint-input-sender 'ignore)
              (comint-input-filter-functions nil)
              (comint-input-filter #'ignore))
          (comint-send-input t t))
        (compilation-forget-errors))
      (python-shell-send-string string process)
      (deactivate-mark)
      t)))

(advice-add 'python-shell-send-region :before-until #'sage--send-region)

;;; Info manual
(defvar-keymap sage-info-mode-map
  "<remap> <Info-goto-node>" #'sage-goto-info-node
  "<remap> <consult-info>" 'sage-consult-info)

(define-minor-mode sage-info-mode
  "Minor mode providing some conveniences for sage info manual."
  :keymap sage-info-mode-map
  (funcall
   (if sage-info-mode #'font-lock-add-keywords #'font-lock-remove-keywords)
   nil
   `((,(rx bol (* space) (group "sage:") " ") 1 'comint-highlight-prompt)
     (,(rx bol (* space) (group (or "AUTHORS" "EXAMPLES" "TESTS" "INPUT"
                                    "OUTPUT" "ALGORITHM" "REFERENCES")
                                ":"))
      1 'bold)
     (,(rx bol (* space) "--" (* space) (group "Class:")) (1 'font-lock-doc-face)
      (,(rx (* space) (group (*? nonl)) (or space eol) (* nonl))
       nil nil (1 'font-lock-type-face)))
     (,(rx bol (* space) "--" (* space) (group "Function:")) (1 'font-lock-doc-face)
      (,(rx (* space) (group (*? nonl)) (or space eol) (* nonl))
       nil nil (1 'font-lock-function-name-face)))
     (,(rx bol (* space) "--" (* space) (group "Method:")) (1 'font-lock-doc-face)
      (,(rx (* space) (group (*? nonl)) (or space eol) (* nonl))
       nil nil (1 'font-lock-function-name-face)))
     (,(rx bol (* space) "--" (* space) (group "Attribute:")) (1 'font-lock-doc-face)
      (,(rx (* space) (group (*? nonl)) (or space eol) (* nonl))
       nil nil (1 'font-lock-variable-name-face)))))
  (if sage-info-mode
      (progn (setq prettify-symbols-alist
                   (with-temp-buffer
                     (if (fboundp #'LaTeX-mode)
                         (LaTeX-mode)
                       (latex-mode))
                     prettify-symbols-alist))
             (cl-callf2 append sage-info-prettify-symbols prettify-symbols-alist)
             (when (and (require 'tex-mode nil t)
                        (fboundp 'tex--prettify-symbols-compose-p))
              (setq prettify-symbols-compose-predicate #'tex--prettify-symbols-compose-p))
             (prettify-symbols-mode)
             (cl-callf append jit-lock-functions '(sage--indirect-jit-lock))
             (jit-lock-refontify))
    (prettify-symbols-mode -1)
    (jit-lock-unregister #'sage--indirect-jit-lock)
    (jit-lock-refontify)))

(defun sage-info ()
  "Open the sage info manual."
  (interactive)
  (info sage-info-manual (get-buffer-create "*sage-info*"))
  (unless sage-info-mode (sage-info-mode)))

(defun sage-goto-info-node (name)
  "Goto the node named NAME in the sage info manual."
  (interactive
   (list (let ((completion-ignore-case t)
               (completions (Info-build-node-completions sage-info-manual)))
           (completing-read "Goto sage node:" completions nil t nil
                            'Info-minibuf-history (python-info-current-symbol)))))
  (sage-info)
  (Info-goto-node (format "(%s)%s" sage-info-manual name)))

(defun sage--indirect-jit-lock (beg end)
  "Do syntax highlighting for code from BEG to END using an indirect buffer."
  (unless (get-buffer " *sage-info-indirect*")
    (with-current-buffer
        (make-indirect-buffer "*sage-info*" " *sage-info-indirect*" nil t)
      (setq-local delay-mode-hooks t)
      (sage-mode)
      (setq-local font-lock-dont-widen t
                  font-lock-support-mode nil
                  font-lock-global-modes nil)))
  (goto-char beg)
  (setq beg (pos-bol))
  (goto-char end)
  (setq end (pos-eol))
  (with-current-buffer (get-buffer " *sage-info-indirect*")
    (widen)
    (goto-char beg)
    (let (rbeg rend indent)
      (while (re-search-forward (rx bol (group (* space)) (group "sage:") " ") end 'move)
        (setq indent (match-string 1))
        (setq rbeg (match-end 0))
        (while (progn (forward-line 1)
                      (and (looking-at-p indent)
                           (not (search-forward "sage:" (pos-eol) 'move)))))
        (unless (eobp) (forward-line -1))
        (goto-char (pos-eol))
        (setq rend (point))
        (narrow-to-region rbeg rend)
        (put-text-property rbeg rend 'font-lock-face nil)
        (font-lock-fontify-region rbeg rend)
        (narrow-to-region beg end)
        (goto-char rend))
      (goto-char beg)
      (while (re-search-forward (rx bol (+ space)) end 'move)
        (setq rbeg (match-beginning 0))
        (setq rend (match-end 0))
        (when (> (- rend rbeg) 4)
          (put-text-property rbeg rend 'display `(space :width ,(/ (* 2 (- rend rbeg)) 5)))))
      (goto-char beg)
      (while (re-search-forward (rx (+ (not " ")) " " (group (* " ")) "#") end 'move)
        (put-text-property (match-beginning 1) (match-end 1) 'invisible t))))
  `(jit-lock-bounds ,beg . ,end))

(defun sage--get-full-name (objname)
  "Get the full name of object with name OBJNAME for sage process."
  (let* ((proc (python-shell-get-process))
         (proc-buf (process-buffer proc)))
    (with-temp-buffer
      (sage--redirect-send
       (format "__SAGE_get_full_name(%s)" objname) (current-buffer) proc nil t)
      (while (not (buffer-local-value 'comint-redirect-completed proc-buf))
        (accept-process-output))
      (goto-char (point-min))
      (buffer-substring-no-properties (point-min) (pos-eol)))))

(defun sage-info-at-point ()
  "Go to the node describing the symbol at point in the sage info manual."
  (interactive)
  (if-let ((sym (python-info-current-symbol))
           (name (string-replace "." " " (sage--get-full-name sym))))
      (if-let ((node (cl-find-if (lambda (str) (string-suffix-p sym (car str)))
                                 (Info-build-node-completions sage-info-manual))))
          (sage-goto-info-node (car node))
        (error "Couldn't find node corresponding to the symbol %s" sym))
    (error "Couldn't determine the name of current symbol")))

;;; Provide
(provide 'sage)
;;; sage.el ends here
