;;; sage-consult.el --- Description -*- lexical-binding: t; -*-
;;; Commentary:
;; Browse sage documentation using consult.
;;; Code:
(require 'consult)
(require 'sage)

(defvar sage-consult--history nil)

;;;###autoload
(defun sage-consult ()
  "A documentation browser for sage objects."
  (interactive)
  (if (sage-busy-p (get-buffer (python-shell-get-buffer)))
      (message "Sage process is busy. Retry once it is done.")
    (let* ((doc-buf (get-buffer-create "*Sage Documentation*" t))
           (proc (python-shell-get-process))
           (table (sage-completion-table proc))
           (comps (all-completions "" table))
           (cached (make-hash-table :test #'equal)))
      (cl-flet
          ((state (action cand)
             (with-current-buffer doc-buf
               (pcase action
                 ('preview (when cand
                             (cl-callf substring-no-properties cand)
                             (when-let (((not (gethash cand cached)))
                                        (cl (all-completions (concat cand ".") table)))
                               (cl-callf append comps cl)
                               (puthash cand t cached))
                             (sage-lookup-doc cand proc)))
                 ('setup (visual-line-mode)
                         (display-buffer doc-buf))
                 ('return (when-let ((win (get-buffer-window doc-buf)))
                            (delete-window win)
                            (kill-buffer doc-buf)))))))
        (unwind-protect
            (consult--read
             comps
             :prompt "Sage Objects: "
             :state #'state
             :preview-key 'any
             :history sage-consult--history
             :require-match t))))))

(defun sage-consult-send-heading-body ()
  "Choose a heading using `consult-outline' and send its body to sage process."
  (interactive)
  (save-excursion (consult-outline)
                  (setq this-command #'sage-send-heading-body)
                  (sage-send-heading-body)))

;;;###autoload
(defun sage-consult-info ()
  "Search sage manual using `consult-info'."
  (interactive)
  (sage-info)
  (cl-letf (((symbol-function 'consult-info--action)
             (lambda (cand)
               (pcase (consult-info--position cand)
                 (`( ,_matches ,pos ,_node ,_bol ,buf)
                  (sage-info)
                  (widen)
                  (goto-char pos)
                  (Info-select-node)
                  (run-hooks 'consult-after-jump-hook))))))
    (consult-info (car sage-info-manual))))

(provide 'sage-consult)
;;; sage-consult.el ends here
