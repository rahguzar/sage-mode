;;; sage-rich.el --- Description -*- lexical-binding: t; -*-
;;; Commentary:
;;
;; Support for viewing latex typset output and images from sage in Emacs.
;;
;;; Code:
(require 'sage)

;;; Variables
(defvar sage-rich-scale nil
  "Scale to use for images containing typset text.")
(defconst sage-rich--temp-dir (make-temp-file "sage-rich-output-" t))
(defvar sage-rich--log-buffer "*sage-rich-log*")

(defvar sage-rich--backend-code
  "from sage.repl.rich_output.output_basic import OutputLatex, OutputPlainText
from sage.repl.rich_output.output_browser import OutputHtml
from sage.repl.rich_output.output_catalog import OutputImagePng, OutputImageSvg
from sage.repl.rich_output.preferences import DisplayPreferences
from sage.repl.rich_output.backend_ipython import BackendIPythonCommandline

class BackendEmacs(BackendIPythonCommandline):

    def default_preferences(self):
        return DisplayPreferences(text='latex' , graphics='vector')

    def _repr_(self):
        return 'Emacs Sage Mode'

    def supported_output(self):
        return [OutputPlainText , OutputLatex , OutputHtml , OutputImageSvg]

    def displayhook(self, plain_text, rich_output):
        if isinstance(rich_output, OutputImageSvg):
            return ({u'text/plain': 'BEGIN_SVG:' + rich_output.svg.filename() + ':END_SVG'}, {})
        elif isinstance(rich_output, OutputHtml):
            return ({'text/plain': 'BEGIN_TEXT:' + plain_text.text.get_str() + ':END_TEXT\\n'
                                 + 'BEGIN_LATEX:' + rich_output.latex.get_str() + ':END_LATEX'} , {})
        else:
            return super(BackendEmacs, self).displayhook(plain_text, rich_output)

get_display_manager().switch_backend(BackendEmacs(), shell=get_ipython())")

(defvar sage-rich--latex-template
  "\\documentclass[border=0.1pt]{standalone}
\\widowpenalties 1 10000
\\raggedbottom
\\usepackage{amsmath,amssymb}
\\usepackage[utf8]{inputenc}
\\usepackage{xcolor}
\\begin{document}
%s
\\end{document}"
  "Template for the latex document.")

;;; Minor mode
;;;###autoload
(define-minor-mode sage-rich-output-mode
  "Minor mode to enable the display of rich output in Sage shell buffers."
  :lighter "Sage Rich Output"
  (if (and sage-rich-output-mode
           (or (image-type-available-p 'svg) (prog1 nil (message "sage-rich-output-mode cannot be enabaled because Emacs does not have svg support.")))
           (or (executable-find "pdflatex") (prog1 nil (message "sage-rich-output-mode cannot be enabaled because pdflatex was not found.")))
           (or (executable-find "dvisvgm") (prog1 nil (message "sage-rich-output-mode cannot be enabaled because dvisvgm was not found."))))
      (progn (add-hook 'comint-output-filter-functions #'sage-rich--output-filter nil t)
             (python-shell-send-string-no-output sage-rich--backend-code))
    (python-shell-send-string-no-output "get_display_manager().switch_backend(BackendIPythonCommandline(), shell=get_ipython())")
    (remove-hook 'comint-output-filter-functions #'sage-rich--output-filter t)))

;;; Internal definitions
(defun sage-rich--output-filter (&rest _args)
  "Output filter to display the rich output."
  (save-excursion
    (goto-char comint-last-input-end)
    (let ((regex (rx bol "BEGIN_" (group-n 1 (* (not ?:))) ?: (group-n 2 (*? anything)) ":END_" (backref 1)))
          (inhibit-read-only t)
          beg data type)
      (while (re-search-forward regex nil t)
        (setq beg (match-beginning 0))
        (setq data (match-string 2))
        (setq type (match-string 1))
        (pcase type
          ("TEXT" (let (tex
                        (end (point)))
                    (re-search-forward regex nil t)
                    (setq type (match-string 1))
                    (setq tex (string-trim (match-string 2)))
                    (delete-region end (point))
                    (when (equal type "LATEX")
                      (delete-region beg end)
                      (insert data)
                      (sage-rich--typeset tex (current-buffer) beg (point)))))
          ("LATEX" (error "LATEX without plain text!"))
          ("SVG" (let* ((image (create-image data 'svg nil :mask 'heuristic
                                             :max-width (- (window-pixel-width (get-buffer-window)) 10)))
                        (rows (max 1 (1- (cdr (image-size image))))))
                   (delete-region beg (point))
                   (insert-sliced-image image " " nil rows)
                   (delete-line))))))))

(defun sage-rich--insert-image (data pos &optional end tex)
  "Insert image given by DATA in at POS in current buffer.
If END is non-nil text between POS and END is replaced by the image.
TEX should be the latex code the image represents."
  (let* ((image (create-image data 'svg t :scale sage-rich-scale
                              :max-width (- (window-pixel-width (get-buffer-window)) 10)))
         (rows (max 1 (1- (cdr (image-size image))))))
    (save-excursion (goto-char pos)
                    (insert-sliced-image
                     image (propertize " " :plain (delete-and-extract-region pos end) :latex tex)
                     nil rows)
                    (delete-line))))

(defun sage-rich--temp-dir-arg (fmt &optional filename)
  "Expand FMT against `sage-rich--temp-dir' or FILENAME in it."
  (format fmt (if filename
                  (expand-file-name filename sage-rich--temp-dir)
                sage-rich--temp-dir)))

(defmacro sage-rich--sentinel (data-p fun &rest args)
  "Return sentinel for processes which will call FUN with ARGS.
As (apply FUN DATA) args if DATA-P is non-nil. Otherwise as (apply fun args)."
  (let ((event-var (make-symbol "event"))
        (proc-var (make-symbol "proc"))
        (proc-buf-var (make-symbol "proc-buf")))
    `(lambda (,proc-var ,event-var)
       (when (and (equal ,event-var "finished\n") ,fun)
         (let ((,proc-buf-var (process-buffer ,proc-var)))
           (apply ,fun ,@(when data-p
                           `((with-current-buffer ,proc-buf-var
                               (buffer-string))))
                  ,@args)
           (when ,proc-buf-var (kill-buffer ,proc-buf-var))))
       (with-current-buffer (get-buffer-create sage-rich--log-buffer)
         (insert (format "%s %s \n" (process-name ,proc-var) ,event-var))))))

(defun sage-rich--dvi-to-svg (name &optional fun &rest args)
  "Convert NAME.dvi to an svg using dvisvgm.
After the conversion is complete FUN is called as (apply FUN DATA ARGS).
DATA is the svg data produced."
  (make-process :name (concat name "-dvisvgm") :buffer (generate-new-buffer name t) :noquery t
                :command (list "dvisvgm" "--bbox=1pt" "--stdout" "--no-fonts"
                               (sage-rich--temp-dir-arg "--tmpdir=%s")
                               (sage-rich--temp-dir-arg "%s.dvi" name))
                :sentinel (sage-rich--sentinel t fun args)
                :stderr (get-buffer-create sage-rich--log-buffer t)))

(defun sage-rich--tex-to-dvi (name &optional fun &rest args)
  "Compile NAME.tex to a dvi.
After the compilation is done FUN is called as (apply FUN DATA ARGS).
DATA is the svg data produced."
  (make-process :name (concat name "-pdflatex") :noquery t
                :command (list "pdflatex" "-interaction=batchmode" "-draftmode" "--output-format=dvi"
                               (sage-rich--temp-dir-arg "--output-directory=%s")
                               (sage-rich--temp-dir-arg "%s.tex" name))
                :stderr (get-buffer-create sage-rich--log-buffer)
                :sentinel (sage-rich--sentinel nil fun args)))

(defun sage-rich--jax-to-tex (str name &optional fun &rest args)
  "Produce a document NAME.tex containing STR in its body.
Afterwards call FUN as (apply FUN DATA ARGS)"
  (with-temp-buffer
    (insert (format sage-rich--latex-template str))
    (write-region (point-min) (point-max)
                  (sage-rich--temp-dir-arg "%s.tex" name)
                  nil 'quiet)
    (apply fun args)))

(defun sage-rich--insert-and-cleanup (data buf pos &optional end tex name)
  "Insert the image and cleanup the temp files with NAME.
DATA, BUF, POS, END and TEX are as in `sage-rich-insert-image'."
  (with-current-buffer buf
    (let ((inhibit-read-only t))
      (sage-rich--insert-image
       (substring data (string-search "<" data)) pos end tex)))
  (mapc #'delete-file (directory-files sage-rich--temp-dir t (rx bos (literal name)))))

(defun sage-rich--typeset (tex buf pos &optional end)
  "Typeset the TEX and insert the resulting image at POS in BUF.
If END is non-nil the image replaces the text between POS and END."
  (let ((name (format "sage-output-%s" (random))))
    (sage-rich--jax-to-tex tex name
                          #'sage-rich--tex-to-dvi name
                          #'sage-rich--dvi-to-svg name
                          #'sage-rich--insert-and-cleanup buf pos end tex name)))

;;; Interactive functions
(defun sage-rich-kill-ring-save (n)
  "Save the LaTeX code for the Nth last output to kill ring."
  (interactive (list (prefix-numeric-value current-prefix-arg)))
  (save-excursion
    (goto-char (point-max))
    (when-let ((lim (progn (comint-previous-prompt (1- n))
                           (line-beginning-position)))
               (pos (progn (comint-previous-prompt 1)
                           (text-property-not-all (point) lim :latex nil)))
               (str (string-trim (get-text-property pos :latex)
                                 (rx "$\\displaystyle" (? (* space)))
                                 (rx (? (* space)) "$"))))
      (kill-new str))))

(defun sage-rich-toggle-plain-text (n)
  "If Nth last input is showing type LaTeX show plaintext or vice versa."
  (interactive (list (prefix-numeric-value current-prefix-arg)))
  (save-excursion
    (goto-char (point-max))
    (if-let ((end (progn (comint-previous-prompt (1- n))
                         (forward-line -1) (line-end-position)))
             (beg (progn (comint-previous-prompt 1)
                         (text-property-not-all (point) end :latex nil)))
             (ov  (cdr (get-char-property-and-overlay beg 'display))))
        (delete-overlay ov)
      (setq ov (make-overlay beg end))
      (overlay-put ov 'display (get-text-property beg :plain)))))

(provide 'sage-rich)
;;; sage-rich.el ends here
