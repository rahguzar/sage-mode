# sage-mode: An alternate integration of SageMath and Emacs.

*sage-mode* is a package for interacting with [SageMath](https://www.sagemath.org/) source files and REPLs from inside Emacs. It leverages the built in Python support in Emacs for most of its functionality as a result you can use keybindings and commands for python in buffers for SageMath.

To associate a SageMath source buffer with a `sage` process buffer run `sage-start` (bound to `C-c C-p`).

*sage-mode* supports standard Emacs autocompletion by providing a CAPF (`completion-at-point-function`) that queries a running `sage` process for completions. The associated process is also used for `eldoc` support.

## Additional features
Other features provided are:
### `outline-minor-mode` support
Any line starting with `###` is recognized as a heading. The body of current heading can be sent to associated `sage` process by running `sage-send-heading-body` (bound to `C-c C-b`).

### LaTeX typesetting of output and inline images
*sage-mode* provides `sage-rich-output-mode` which enables typeset output from the `sage` process and inline display of images.

To enable it by default put
```lisp
(setq-default sage-rich-output t)
```
in your config.

### `sage-consult`
`sage-consult` is a command to explore available `sage` objects using `completing-read`. It requires the [consult](https://github.com/minad/consult) which is an optional dependency of *sage-mode*.

## Comparison to `sage-shell-mode`
The SageMath project provides a package for Emacs called [`sage-shell-mode`](https://github.com/sagemath/sage-shell-mode) which works very well. *sage-mode* is an experiment to use the builtin `python` support of Emacs for almost everything. If some feature of `sage-shell-mode` is missing check if there is an equivalent `python` feature in Emacs. If there is, chances are high that it will work with *sage-mode*.
